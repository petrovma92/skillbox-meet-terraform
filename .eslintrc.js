module.exports = {
    "env": {
        "browser": true
    },
    "extends": ["plugin:react/recommended"],
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 12,
        "sourceType": "module"
    },
    "plugins": ["react"],
    "rules": {
	"react/react-in-jsx-scope": "off",
	"linebreak-style": [
	    "error",
	    "unix"
	],
	"quotes": [
	    "error",
	    "single"
	],
	"semi": [
	    "error",
	    "always"
	]
    }
};
